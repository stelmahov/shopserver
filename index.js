const express = require('express')
const app = express()
const products = require('./products');
var path = require('path');

app.disable('etag');
const port = 3006

app.get(['/', '/home', '/login', '/products', '/registration', '/about-us'], (request, response) => {
    response.status(200);
    response.sendFile(path.join(__dirname + '/public/index.html'));
})

app.post('/products', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status(200);
    response.send(products)
})

app.post('/purchase', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status(200);
    response.send({message:"success"});
})

var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

app.listen(port, (err) => {
    if(err) {
        return console.log('something washappend', err)
    }
    console.log(`server is listening on ${port}`)
})